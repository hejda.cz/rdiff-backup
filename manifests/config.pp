class rdiff_backup::config(
	$user,
	$folder
) {
	user { $user:
	  comment    => 'Rdiff-backup user',
	  home       => $folder,
	  ensure     => present,
	  managehome => true,
	  shell      => '/bin/sh',
	  uid        => '172',
	  gid        => '172'
	}
	group { $user:
		gid => '172',
	}
	Group[$user]->User[$user]
}