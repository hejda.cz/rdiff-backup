class rdiff_backup ( 
    $bck_dir = "/var/backup",
    $bck_dir_type = "directory",
    $log_dir = "/_log",
    $bin_dir = "/_bin",
    $user = "rdbackup",
    $retention = "1W",
    $hosts = [],
    $backup_dirs = ['etc','var'],
    $var_exclude_dirs = [
      'backup',
      'cache',
      'lib/apt/lists',
      'lib/docker',
      'lib/elastic-agent',
      'lib/filebeat',
      'lib/gitlab-runner',
      'lib/grafana',
      'lib/logstash',
      'lib/lxcfs',
      'lib/mysql',
      'lib/mongodb',
      'lib/php/sessions',
      'lib/postgresql',
      'log',
      'opt',
      'spool',
      'tmp'
    ],
    $etc_exclude_dirs = ['init'],
    $pre_backup_hook_path = false,
    $post_backup_hook_path = false,
    $pkg_version = "1.2.8-7",
    $priv_key,
    $pub_key,
    $cron_hour = "*"
  ) {
  class { "rdiff_backup::packages":
    version => $pkg_version,
  } ->
  class { "rdiff_backup::config":
    user   => $user,
    folder => $bck_dir,
  } ->
  file { "${bck_dir}":
    ensure => $bck_dir_type,
    owner  => $user,
  } ->
  file { "${bck_dir}/.ssh":
    ensure => directory,
    owner  => $user,
    mode   => '0600',
  } ->
  file { "${bck_dir}/.ssh/id_rsa":
    ensure => file,
    owner  => $user,
    mode   => '0600',
    content => $priv_key,
  } ->
  file { "${bck_dir}/.ssh/authorized_keys":
    ensure  => file,
    owner   => $user,
    mode    => '0600',
    content => $pub_key,
  } ->
  file { "${bck_dir}/${::fqdn}":
    ensure => directory,
    owner  => $user,
  } ->
  file { "${bck_dir}/${::fqdn}${bin_dir}":
    ensure => directory,
    owner  => $user,
  } ->
  file { "${bck_dir}/${::fqdn}${bin_dir}/run.sh":
    ensure  => file,
    content => template("rdiff_backup/basic_backup.sh.erb"),
    owner   => 'root',
    mode    => '0755'
  } ->
  file { "${bck_dir}/${::fqdn}${bin_dir}/etc_exclude":
    ensure  => file,
    content => template("rdiff_backup/excludes/etc_exclude.erb"),
    owner   => $user,
  } ->
  file { "${bck_dir}/${::fqdn}${bin_dir}/var_exclude":
    ensure  => file,
    content => template("rdiff_backup/excludes/var_exclude.erb"),
    owner   => $user,
  }
  if( $pre_backup_hook_path ) {
    file { "${bck_dir}/${::fqdn}${bin_dir}/${::fqdn}_pre.sh":
      ensure  => file,
      content => template($pre_backup_hook_path),
      owner   => $user,
      before  => Cron['rdbackup'],
      require => File["${bck_dir}/${::fqdn}${bin_dir}/var_exclude"],
      mode    => '0700'
    }
  }
  if( $post_backup_hook_path ) {
    file { "${bck_dir}/${::fqdn}${bin_dir}/${::fqdn}_post.sh":
      ensure  => file,
      content => template($post_backup_hook_path),
      owner   => $user,
      before  => Cron['rdbackup'],
      require => File["${bck_dir}/${::fqdn}${bin_dir}/var_exclude"],
      mode    => '0700'
    }
  }


  $period = 60/4

  $minute1 = fqdn_rand($period)

  cron { 'rdbackup':
    command  => "${bck_dir}/${::fqdn}${bin_dir}/run.sh",
    user     => 'root',
    month    => '*',
    monthday => '*',
    hour     => $cron_hour,
    minute   => $minute1,
  }

}