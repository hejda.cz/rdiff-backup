class rdiff_backup::packages(
	$version
) {
	package { 'rdiff-backup':
		ensure => $version,
	}
}